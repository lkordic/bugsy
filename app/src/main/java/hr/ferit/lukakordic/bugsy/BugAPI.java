package hr.ferit.lukakordic.bugsy;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Luka on 27.4.2017..
 */

public interface BugAPI {
    @GET("rss/vijesti")
    Call<RssFeed> loadRssFeed();
}
