package hr.ferit.lukakordic.bugsy;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends Activity implements Callback<RssFeed>, SwipeRefreshLayout.OnRefreshListener {

    private static final String BUG_API = "http://www.bug.hr/";

    private SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView rvBugNews;
    RssAdapter rssAdapter;
    RecyclerView.LayoutManager layoutManager;
    Spinner spinnerCategories;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }

    private void setUpUI() {
        this.spinnerCategories = (Spinner) findViewById(R.id.spinnerCategory);
        this.swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        Context context = getApplicationContext();
        this.rvBugNews = (RecyclerView) findViewById(R.id.rvBugNews);
        rvBugNews.addItemDecoration(new SimpleDividerItemDecoration(this));
        this.rssAdapter = new RssAdapter(this.loadNews());
        this.layoutManager = new LinearLayoutManager(context);
        this.rvBugNews.setLayoutManager(layoutManager);
        this.rvBugNews.setAdapter(rssAdapter);
        ArrayAdapter<CharSequence> adapter_spinner = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        adapter_spinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(adapter_spinner);
    }

    private ArrayList<Article> loadNews() {
        ArrayList<Article> articles = new ArrayList<>();
        //TODO: get data from Bug.hr
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BUG_API)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        BugAPI api = retrofit.create(BugAPI.class);
        Call<RssFeed> call = api.loadRssFeed();
        call.enqueue(this);
        return articles;
    }

    @Override
    public void onResponse(Call<RssFeed> call, Response<RssFeed> response) {
        List<Article> articles = response.body().getArticleList();
        RssAdapter adapter = new RssAdapter((ArrayList<Article>) articles);
        this.rvBugNews.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<RssFeed> call, Throwable t) {
        Log.d("Fail", t.getMessage());
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {

            @Override public void run() {
                rssAdapter.clear();
                rssAdapter.addAll(rssAdapter.articles);
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);

    }
}
