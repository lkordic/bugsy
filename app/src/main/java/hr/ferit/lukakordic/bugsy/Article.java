package hr.ferit.lukakordic.bugsy;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;


/**
 * Created by Luka on 27.4.2017..
 */

@Root(name = "item", strict = false)
public class Article {
    @Element(name = "title")
    private String title;
    @Element(name = "category")
    private String category;
    @Element(name = "description")
    private String description;
    @Element(name = "pubDate")
    private String pubDate;
    @Attribute(name = "url")
    @Path("enclosure") private String imageUrl;
    @Element(name = "link")
    private String link;

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
