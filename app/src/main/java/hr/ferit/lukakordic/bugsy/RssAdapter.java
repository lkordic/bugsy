package hr.ferit.lukakordic.bugsy;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luka on 25.4.2017..
 */

public class RssAdapter extends RecyclerView.Adapter<RssAdapter.ViewHolder> {

    ArrayList<Article> articles;

    public RssAdapter(ArrayList<Article> articles) {
        this.articles = articles;
    }

    @Override
    public RssAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View articleView = inflater.inflate(R.layout.rss_object_layout, parent, false);
        ViewHolder RssObjectViewHolder = new ViewHolder(articleView);
        return RssObjectViewHolder;
    }

    @Override
    public void onBindViewHolder(RssAdapter.ViewHolder holder, int position) {
        Article article = this.articles.get(position);
        holder.tvArticleTitle.setText(article.getTitle());
        holder.tvArticleCategory.setText(article.getCategory());
        holder.tvArticleDescription.setText(article.getDescription());
        holder.tvPubDate.setText(article.getPubDate());
        holder.tvLink.setText(article.getLink());
        Picasso.with(holder.itemView.getContext())
                .load(article.getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.ivArticleImage);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvArticleTitle, tvArticleCategory, tvArticleDescription, tvPubDate, tvLink;
        ImageView ivArticleImage;

        public ViewHolder(View itemView) {
            super(itemView);
            this.ivArticleImage = (ImageView) itemView.findViewById(R.id.ivRssImage);
            this.tvArticleTitle = (TextView) itemView.findViewById(R.id.tvRssTitle);
            this.tvArticleCategory = (TextView) itemView.findViewById(R.id.tvRssCategory);
            this.tvArticleDescription = (TextView) itemView.findViewById(R.id.tvRssDescription);
            this.tvPubDate = (TextView) itemView.findViewById(R.id.tvPubDate);
            this.tvLink = (TextView) itemView.findViewById(R.id.tvLink);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent intent = new Intent(context, ReadArticle.class);
            intent.putExtra("Title", this.tvArticleTitle.getText().toString());
            intent.putExtra("Category", this.tvArticleCategory.getText().toString());
            intent.putExtra("Description", this.tvArticleDescription.getText().toString());
            intent.putExtra("Link", this.tvLink.getText().toString());
            context.startActivity(intent);
        }
    }

    public void clear(){
        this.articles.clear();
        notifyDataSetChanged();
    }
    public void addAll(ArrayList<Article> articles){
        this.articles.addAll(articles);
        notifyDataSetChanged();
    }
}
