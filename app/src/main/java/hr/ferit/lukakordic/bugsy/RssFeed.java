package hr.ferit.lukakordic.bugsy;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

import static android.R.attr.name;

/**
 * Created by Luka on 24.4.2017..
 */

@Root(name = "rss", strict = false)
public class RssFeed {

    @Element(name = "title")
    @Path("channel") private String channelTitle;

    @ElementList(name = "item", inline = true)
    @Path("channel") private List<Article> articleList;

    public String getChannelTitle() {
        return channelTitle;
    }

    public List<Article> getArticleList() {
        return articleList;
    }
}
