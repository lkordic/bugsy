package hr.ferit.lukakordic.bugsy;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ReadArticle extends Activity implements View.OnClickListener {

    TextView tvReadTitle, tvReadCategory, tvReadDescription, tvReadMore;
    String title, category, description, readmore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_article);
        this.setUpUi();
    }

    private void setUpUi() {
        this.tvReadTitle = (TextView) findViewById(R.id.tvReadTitle);
        this.tvReadCategory = (TextView) findViewById(R.id.tvReadCategory);
        this.tvReadDescription = (TextView) findViewById(R.id.tvReadDescription);
        this.tvReadMore = (TextView) findViewById(R.id.tvReadMore);
        Intent intent = getIntent();
        this.title = intent.getStringExtra("Title");
        this.category = intent.getStringExtra("Category");
        this.description = intent.getStringExtra("Description");
        this.readmore = intent.getStringExtra("Link");
        this.tvReadTitle.setText(title);
        this.tvReadCategory.setText(category);
        this.tvReadDescription.setText(description);
        this.tvReadMore.setText(readmore);

        this.tvReadMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvReadMore.getText().toString()));
        startActivity(browserIntent);
    }
}
