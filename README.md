# Bugsy #
## Bug RSS reader ##
### Opis ###
Aplikacija čita rss feedove sa Bug.hr stranice u XML obliku i parsira ih u java objekte. Vijesti su prikazane u RecyclerView-u a klikom na svaku pojedinu vijest otvara se nova aktivnost sa koje se može, klikom na link, pročitati vijesti na Bug-ovoj internet stranici.

### Pomoćni materijali ###
* [Retrofit](Lhttp://www.vogella.com/tutorials/Retrofit/article.htmlink URL)
* [Item Divider](http://stackoverflow.com/questions/31242812/how-can-a-divider-line-be-added-in-an-android-recyclerview)
* [Swipe refresh](http://stackoverflow.com/questions/37976554/implementing-swipe-refresh-for-recyclerview-in-android)
* [Recycler onClick](http://stackoverflow.com/questions/24471109/recyclerview-onclick)
![Screenshot_2017-04-27-22-57-11[1].png](https://bitbucket.org/repo/kMM7Ey5/images/3996858596-Screenshot_2017-04-27-22-57-11%5B1%5D.png)
![Screenshot_2017-04-27-22-57-37[1].png](https://bitbucket.org/repo/kMM7Ey5/images/1979452676-Screenshot_2017-04-27-22-57-37%5B1%5D.png)![Screenshot_2017-04-27-22-58-19[1].png](https://bitbucket.org/repo/kMM7Ey5/images/2375310815-Screenshot_2017-04-27-22-58-19%5B1%5D.png)![Screenshot_2017-04-27-22-59-17[1].png](https://bitbucket.org/repo/kMM7Ey5/images/2524564931-Screenshot_2017-04-27-22-59-17%5B1%5D.png)